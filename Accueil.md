# Italie

## Générailtés :

L'Italie apporte une contribution très importante à la civilisation occidentale : elle est notamment le berceau de la civilisation étrusque, de la Grande-Grèce, de l'Empire romain, du Saint-Siège, des républiques maritimes, de l'humanisme et de la Renaissance. Existant en tant qu'État unitaire depuis 1861 à la suite du Risorgimento (Renaissance ou Résurrection) mené par le royaume de Sardaigne, l'Italie est une république depuis l'abolition par référendum de la monarchie italienne en 1946. Elle est membre fondateur de l'Union européenne et de la zone euro. 

## Culture :

L’Italie a joué un rôle majeur dans l’histoire de l’Occident. Important foyer culturel, berceau de la civilisation étrusque, centre de l’Empire romain… le pays a aussi été bercé par de nombreux courants artistiques. Il en résulte un patrimoine culturel incroyablement riche que vous ne retrouverez nulle part ailleurs. En témoigne la profusion de musées, de palais et d’églises dans tout le pays. Florence, berceau de la Renaissance, est à elle seule une belle vitrine historique et culturelle où les bijoux d’architecture se succèdent. Du Ponte Vecchio au palais Bartolini, le visiteur a droit à un inoubliable voyage dans le temps. L’Italie est aussi indissociable des grands noms de l’art, comme Da Vinci ou encore Michel-Ange dont les œuvres continuent d’émouvoir les visiteurs de la Place du Capitole à Rome. Durant le mois d’avril, la Semaine de la Culture est un prétexte pour vivre et découvrir l’Italie autrement. Certains musées accueillent même les visiteurs gratuitement. Le Mai des monuments à Naples est un autre évènement qui donne lieu à de nombreuses surprises, telles que des visites guidées, des concerts, etc.

L’autre visage du pays, c’est le catholicisme qui a indéniablement forgé son identité. Chaque ville possède son saint-patron qui fait l’objet de toutes sortes de rituels. Cette facette religieuse s’apprécie dans un décor grandiose, presque théâtral au Vatican, avec ses musées, la célèbre chapelle Sixtine et la basilique Saint-Pierre.

## L'art :

Depuis la plus haute Antiquité, l'Italie a apporté une contribution essentielle à l'art et à l'architecture en Occident. L'art italien trouve ses racines dans les traditions esthétiques de la Rome classique, elles-mêmes issues des civilisations préromaines de la péninsule italienne, notamment grecque et étrusque. L'art italien célèbre essentiellement la vie et les réalisations humaines, tout en affirmant l'harmonie des relations entre l'homme et la nature. Malgré les divisions politiques que connut la péninsule de la chute de l'Empire romain occidental (ive s. après J.-C.) jusqu'à la formation de la nation italienne, au milieu du xixe s., l'existence d'un héritage classique commun et l'hégémonie de l'Église catholique romaine ont induit une certaine unité stylistique, technique, fonctionnelle et thématique de la production artistique. Toutefois, si cette unité permet de distinguer à coup sûr une œuvre italienne d'une œuvre flamande ou espagnole, les divisions de l'Italie en une mosaïque mouvante d'États jouant le rôle de centres culturels, telles la Toscane ou la Lombardie, et de villes, comme Venise ou Rome, ont suscité l'apparition de différentes écoles, à travers lesquelles se sont affirmées des traditions et des tendances artistiques locales.




