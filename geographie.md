# Italie
L'Italie a la forme générale d’une botte, d’une superficie totale de 301 263 km2. Elle est constituée d'une partie continentale, péninsulaire et insulaire, dont les trois principales îles sont la Sicile (qui aurait la forme d'une tête de crocodile), la Sardaigne et l’île d’Elbe. 
Les dimensions maximum sont de 1 330 km, du nord au sud, de la Tête Jumelle Occidentale à Lampedusa, et de 630 km, d’est en ouest, du mont Thabor au mont Nevoso.

